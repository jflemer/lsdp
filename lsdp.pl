#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use IO::Socket;
use IO::Socket::Timeout;
use JSON;
use POSIX qw(strftime);

$| = 1;

my $BROADCAST_ADDR = '192.168.1.255';

my $LSDP_PORT = 11430;
my $LSDP_INTERVAL = 60;
my $LSDP_FUZZ = 6;
my $LSDP_DELAY = 0.75;

my $PKT_HEADER = 'CA4Ca*';
my @PKT_HEADER_VARS = qw/length magic version rest/;
my $MESSAGE_HEADER = 'CA';
my @MESSAGE_HEADER_VARS = qw/length type/;
my $ANNOUNCE_MSG = $MESSAGE_HEADER . 'C/aC/aCa*';
my @ANNOUNCE_MSG_VARS = (@MESSAGE_HEADER_VARS, qw/node_id address nrec rest/);
my $ANNOUNCE_RECORD = 'nCa*';
my @ANNOUNCE_RECORD_VARS = qw/class ntxt rest/;
my $TXT_RECORD = 'C/AC/Aa*';
my @TXT_RECORD_VARS = qw/key value rest/;
my $QUERY_MSG = $MESSAGE_HEADER . 'C/na*';
my @QUERY_MSG_VARS = (@MESSAGE_HEADER_VARS, qw/classes rest/);

my %DEV = (
  class => 1,
  address => "192.168.2.15",
  name => "PULSE FLEX 2i-2345",
  version => "3.20.45",
  model => "P125",
  node_id => pack("H*", "905682012345"),
  port => 11000,
);

my %MESSAGES = (
  A => 'announce',
  D => 'delete',
  Q => 'query',
  R => 'response',
);

my $TEST = '
  06 4c 53 44 50 01 76 41 06 90 56 82 01 23 45 04
  c0 a8 02 0f 02 00 01 04 04 6e 61 6d 65 12 50 55
  4c 53 45 20 46 4c 45 58 20 32 69 2d 32 33 34 35
  04 70 6f 72 74 05 31 31 30 30 30 05 6d 6f 64 65
  6c 04 50 31 32 35 07 76 65 72 73 69 6f 6e 07 33
  2e 31 34 2e 32 36 00 04 02 04 6e 61 6d 65 12 50
  55 4c 53 45 20 46 4c 45 58 20 32 69 2d 32 33 34
  35 04 70 6f 72 74 05 31 31 34 33 31';
$TEST =~ s/\s+//g;
$TEST = pack('H*', $TEST);

sub LOG {
  my $ts = strftime "%Y-%m-%dT%H:%M:%SL", localtime;
  print "==> [$ts] @_\n";
}

sub fmtjoin {
  join('', grep { defined($_) } @_);
}

sub packx {
  my $fmt = shift;
  my @fmt = $fmt =~ m,([.A-Z@][<>!]?)(?:\[?(\d+|\*)\]?)?|(/),gi;
  my $check = fmtjoin(@fmt);
  die "unable to parse pack format - $fmt" unless $check eq $fmt;
  #print Dumper([ $fmt, \@_ ]);
  my $bin = '';
  my $adv = 3;
  for (my $i = 0; $i < @fmt; 0) {
    my (@fmts, @vals, $n, $fmtadv);
    if ($i+3*$adv-1 <= $#fmt && defined($fmt[$i+2*$adv-1]) && $fmt[$i+2*$adv-1] eq '/') {
      @fmts = @fmt[$i..$i+3*$adv-1];
      $n = 1;
      if (ref($_[0]) eq 'ARRAY') {
        @vals = @{$_[0]};
      } else {
        @vals = ($_[0]);
      }
      $fmtadv = 3*$adv;
    } else {
      @fmts = @fmt[$i..$i+$adv-1];
      if ($fmts[0] =~ m/^[azbhpu]/i) {
        $n = 1;
      } elsif ($fmts[0] =~ m/^[\@x]/i) {
        $n = 0;
      } else {
        $n = $fmts[1];
        $n = 1 unless defined $n;
        $n = @_ if $n eq '*';
      }
      @vals = @_[0..$n-1] if $n;
      $fmtadv = $adv;
    }
    @_ = @_[$n..$#_] if $n;
    $i += $fmtadv;
    #print Dumper({ fmt=>\@fmts, n=>$n, vals=>\@vals, adv=>$fmtadv, rest=>\@_ });
    $bin .= pack(fmtjoin(@fmts), @vals);
  }
  #print Dumper({ bin=>unpack('H*', $bin) });
  return $bin;
}

sub unpackx {
  my $fmt = shift;
  my $buf = shift;
  $buf = "$buf";
  my @fmt = $fmt =~ m,([.A-Z@][<>!]?)(?:\[?(\d+|\*)\]?)?|(/),gi;
  my $check = fmtjoin(@fmt);
  die "unable to parse pack format - $fmt" unless $check eq $fmt;
  my $adv = 3;
  my @oarr = ();
  my @arr = ();
  for (my $i = 0; $i < @fmt; 0) {
    my $fmtadv = $adv;
    my $as_array = 0;
    if ($i+3*$adv-1 <= $#fmt && defined($fmt[$i+2*$adv-1]) && $fmt[$i+2*$adv-1] eq '/') {
      $fmtadv = 3*$adv;
      if ($fmt[$i+2*$adv] =~ m/[cslqinvjfdpw]/) {
        $as_array = 1;
      }
    }
    my $fmtx = fmtjoin(@fmt[$i..$i+$fmtadv-1]);
    my @arrx = unpack($fmtx, $buf);
    my $len = length(pack($fmtx, @arrx));
    $buf = substr($buf, $len);
    if ($as_array) {
      push @arr, [ @arrx ];
    } else {
      push @arr, @arrx;
    }
    push @oarr, @arrx;
    #print Dumper({ len=>$len, fmtx=>$fmtx, arrx=>\@arrx, buf=>unpack('H*', $buf), arr=>\@arr, i=>$i, adv=>$fmtadv });
    $i += $fmtadv;
  }
  #print Dumper({ buf=>unpack('H*', $buf), fmt=>\@fmt, oarr=>\@oarr, arrx=>\@arr });
  return @arr;
}

sub decode {
  my $buf = shift;
  my %packet;
  @packet{@PKT_HEADER_VARS} = unpackx($PKT_HEADER, $buf);
  #print STDERR "(buf) HEX: " . unpack('H*', $buf) . "\n";
  #print STDERR "(pkt) HEX: " . unpack('H*', substr($buf, 0, $packet{'length'})) . "\n";
  $buf = $packet{'rest'}; delete $packet{'rest'};
  $packet{'messages'} = [];
  while (length($buf)) {
    #print STDERR "(buf) HEX: " . unpack('H*', $buf) . "\n";
    my $msg = {};
    @{$msg}{@MESSAGE_HEADER_VARS} = unpackx($MESSAGE_HEADER, $buf);
    #print STDERR "(msg) HEX: " . unpack('H*', substr($buf, 0, $msg->{'length'})) . "\n";
    my $type = $MESSAGES{$msg->{'type'}};
    if ($type) {
      my $fn = "decode_$type";
      no strict 'refs';
      ($msg, $buf) = &$fn($buf);
    } else {
      print STDERR "WARN: Unknown message: '" . $msg->{'type'} . "'\n";
      $msg->{'bytes'} = substr($buf, 2, $msg->{'length'} - 2);
      $buf = substr($buf, $msg->{'length'});
    }
    push @{$packet{'messages'}}, $msg;
  }
  \%packet;
}

sub decode_announce {
  my $buf = shift;
  my $msg = {};
  @{$msg}{@ANNOUNCE_MSG_VARS} = unpackx($ANNOUNCE_MSG, $buf);
  $buf = $msg->{'rest'}; delete $msg->{'rest'};
  ($msg->{'node_id'} = unpack('H*', $msg->{'node_id'})) =~ s/(..)(?=.)/$1:/g;
  eval {
    $msg->{'address'} = inet_ntoa($msg->{'address'});
  };
  $msg->{'records'} = [];
  foreach (1..$msg->{'nrec'}) {
    my $rec = {};
    @{$rec}{@ANNOUNCE_RECORD_VARS} = unpackx($ANNOUNCE_RECORD, $buf);
    $buf = $rec->{'rest'}; delete $rec->{'rest'};
    $rec->{'txt'} = {};
    foreach (1..$rec->{'ntxt'}) {
      my %txt;
      @txt{@TXT_RECORD_VARS} = unpackx($TXT_RECORD, $buf);
      $buf = $txt{'rest'}; delete $txt{'rest'};
      $rec->{'txt'}->{$txt{'key'}} = $txt{'value'};
    }
    push @{$msg->{'records'}}, $rec;
  }
  return ($msg, $buf);
}

sub encode_packet {
  my %opt = ( 'magic' => 'LSDP', 'version' => 1, 'rest' => '' );
  $opt{'length'} = length(encode_generic($PKT_HEADER, \@PKT_HEADER_VARS, \%opt));
  $opt{'rest'} = join('', @_);
  return encode_generic($PKT_HEADER, \@PKT_HEADER_VARS, \%opt);
}

sub decode_query {
  my $buf = shift;
  my $msg = {};
  @{$msg}{@QUERY_MSG_VARS} = unpackx($QUERY_MSG, $buf);
  #print Dumper({query => $msg});
  $buf = $msg->{'rest'}; delete $msg->{'rest'};
  return ($msg, $buf);
}

sub encode_query {
  my (%args) = @_;
  $args{'type'} = 'Q';
  return encode_generic($QUERY_MSG, \@QUERY_MSG_VARS, \%args);
}

sub encode_announce {
  my $opts = shift;
  my $id = $opts->{'node_id'};
  $id = inet_aton($opts->{'address'}) unless defined $id;
  my @rec = ();
  my @txt = map { encode_generic($TXT_RECORD, \@TXT_RECORD_VARS, { key => $_, value => "$opts->{$_}", rest => '' }) } qw/model name port version/;
  push @rec, encode_generic($ANNOUNCE_RECORD, \@ANNOUNCE_RECORD_VARS, {
      'class' => $opts->{'class'}, ntxt => scalar(@txt), rest => join('', @txt)
    });
  my %dbg = ('name' => $opts->{'name'}, port => '11431');
  @txt = map { encode_generic($TXT_RECORD, \@TXT_RECORD_VARS, { key => $_, value => "$dbg{$_}", rest => '' }) } keys %dbg;
  push @rec, encode_generic($ANNOUNCE_RECORD, \@ANNOUNCE_RECORD_VARS, {
      'class' => 4, ntxt => scalar(@txt), rest => join('', @txt)
    });
  return encode_generic($ANNOUNCE_MSG, \@ANNOUNCE_MSG_VARS, {
      type => 'A',
      node_id => $id,
      address => inet_aton($opts->{'address'}),
      nrec => scalar(@rec),
      rest => join('', @rec)
    });
}

sub encode_generic {
  my $fmt = shift;
  my $vars = shift;
  my $vals = shift;
  my $len = 'length';
  my $rest = 'rest';
  if (grep { $_ eq $len } @$vars) {
    if (!exists($vals->{$len})) {
      $vals->{$len} = 0;
    }
  }
  if (grep { $_ eq $rest } @$vars) {
    if (!exists($vals->{$rest})) {
      $vals->{$rest} = '';
    }
  }
  my @arr = $vals->@{@$vars};
  my $msg = packx($fmt, @arr);
  if (exists($vals->{$len}) && $vals->{$len} == 0) {
    $vals->{$len} = length($msg);
    @arr = $vals->@{@$vars};
    $msg = packx($fmt, @arr);
  }
  #print Dumper({ fmt=>$fmt, arr=>\@arr, msg=>unpack('H*', $msg) });
  return $msg;
}

sub listener {
  my $max = shift;
  $max = 0 unless defined $max;
  my $timeout = shift;
  $timeout = 0 unless defined $timeout;
  my $sock = IO::Socket::INET->new(
    Proto => 'udp',
    LocalAddr => '0.0.0.0',
    LocalPort => $LSDP_PORT,
    ReuseAddr => 1,
    ReusePort => 1,
    Broadcast => 1
  ) or die "socket: $@";
  if ($timeout) {
    IO::Socket::Timeout->enable_timeouts_on($sock);
    $sock->read_timeout($timeout);
  }
  my $packet;
  my $msg;
  my $count = 0;
  while ($sock->recv($packet, 64*1024)) {
    my ($peer_port, $peer_addr) = sockaddr_in($sock->peername);
    my $ts = scalar(localtime);
    my $peer = join(':', inet_ntoa($peer_addr), unpack('n', pack('S', $peer_port)));
    my $len = length($packet) . " bytes";
    my $hex = unpack('H*', $packet);
    print STDERR join(' ', $ts, $peer, $len, $hex), "\n";
    $msg = decode($packet);
    $packet = undef;
    print JSON->new->utf8->pretty->canonical->encode($msg);
    last if (++$count >= $max && $max > 0);
  }
  return $msg;
}

sub sender {
  my $packet = shift;
  my $addr = shift;
  $addr = $BROADCAST_ADDR unless defined $addr;
  my $sock = IO::Socket::INET->new(
    Proto => 'udp',
    LocalAddr => '0.0.0.0',
    PeerPort => $LSDP_PORT,
    PeerAddr => $addr,
    ReuseAddr => 1,
    ReusePort => 1,
    Broadcast => 1
  ) or die "socket: $@";
  $sock->send($packet);
}

sub query {
  my $msg = encode_query('classes' => \@_);
  my $pkt = encode_packet($msg);
  my $res = decode($pkt);
  print JSON->new->utf8->pretty->canonical->encode($res);
  sender($pkt);
}

sub announce {
  my $msg = encode_announce(@_);
  my $pkt = encode_packet($msg);
  my $res = decode($pkt);
  print JSON->new->utf8->pretty->canonical->encode($res);
  sender($pkt);
}

sub responder {
  while (1) {
    my $timeout = $LSDP_INTERVAL + (rand()-0.5)*$LSDP_FUZZ;
    LOG "responder - listening (timeout=$timeout) ...";
    my $msg = listener(1, $timeout);
    if (defined($msg)) {
      next unless grep { $_->{'type'} eq 'Q' } @{$msg->{'messages'}};
      foreach (1..3) {
        sleep(rand($LSDP_DELAY));
        LOG "responder - announcing (queried) ...";
        announce(@_);
      }
    } else {
        LOG "responder - announcing (unsolicited) ...";
        announce(@_);
    }
  }
}

if (grep { $_ eq '-t' } @ARGV) {
  my $m = decode($TEST);
  print JSON->new->utf8->pretty->canonical->encode($m);
  #my $t = packx($QUERY_MSG, 0, 'Q', [1..4, 0xffff], 'xxx'); print unpack('H*', $t), "\n";
  exit;
}

if (grep { $_ eq '-l' } @ARGV) {
  LOG "listening...";
  listener();
  exit;
}

$BROADCAST_ADDR = $ARGV[1] if $ARGV[1];
$DEV{'address'} = $ARGV[2] if $ARGV[2];
$DEV{'name'} = $ARGV[3] if $ARGV[3];
$DEV{'node_id'} = pack("H*", $ARGV[4]) if $ARGV[4];
$DEV{'version'} = $ARGV[5] if $ARGV[5];
$DEV{'model'} = $ARGV[6] if $ARGV[6];

if (grep { $_ eq '-q' } @ARGV) {
  LOG "sending query...";
  #query(1..4, 0xffff);
  query(0xffff);
  exit;
}
if (grep { $_ eq '-a' } @ARGV) {
  LOG "sending announce...";
  announce(\%DEV);
  exit;
}
if (grep { $_ eq '-r' } @ARGV) {
  LOG "running in responder mode";
  responder(\%DEV);
  exit;
}
exit 64;
