Lenbrook Service Discovery Protocol (LSDP) App
==============================================

Quick and dirty app to listen for or advertise services with LSDP.  This can be
used to announce Bluesound devices on a different subnet.

Listener
--------

    docker run -d --net=host --restart=unless-stopped --name=lsdp lsdp

Query
-----

    docker run --rm -it --net=host lsdp -q 192.168.1.255

Responder (Proxy)
-----------------

    docker run -d --net=host --restart=unless-stopped --name=lsdp lsdp -r $BROADCAST $IP_ADDR "$NAME" "$NODE_ID"
    docker run -d --net=host --restart=unless-stopped --name=lsdp lsdp -r 192.168.1.255 192.168.2.15 "Living Room" "905682012345"

mDNS/Manual Discovery & Playback Testing
----------------------------------------

    for i in musc musp sovi-mfg sovi-keypad musz remote-web-ui mush; do avahi-browse -tr _$i._tcp; done
    curl http://192.168.2.15:11000/SyncStatus
    curl http://192.168.2.15:11000/Play?url=https%3A//somafm.com/nossl/thetrip130.pls

Reference
---------

 * https://content-bluesound-com.s3.amazonaws.com/uploads/2022/07/BluOS-Custom-Integration-API-v1.5.pdf
