FROM perl:5-slim

RUN cpanm --notest JSON IO::Socket::Timeout

COPY lsdp.pl /usr/bin/

EXPOSE 11430/udp

ENTRYPOINT ["perl", "/usr/bin/lsdp.pl"]
CMD ["-l"]
